#include <12F675.h>
#use delay(clock = 4M)
#fuses INTRC_IO,NOMCLR,NOPROTECT,PUT,NOBROWNOUT
#byte GPIO =  0x05// Direcci?n del GPIO PORT
#byte TRISIO =  0x85//Direccion del TRISIO
long distanciaA=0;
long distanciaB=0;
//int8 timer=0;


void inicialisarPuertos()
{
output_a(0);
output_high(PIN_A2);
output_high(PIN_A4);
setup_comparator(NC_NC_NC_NC); 
setup_adc_ports( NO_ANALOGS ); 
setup_adc( ADC_OFF ); 
set_timer1(0);
}

void triggearSensor()
{ 
    output_high(PIN_A0);
    delay_us(11);// cambiar a 10 us 
    output_low(PIN_A0);
}

long siTengoEcoCalculoDistancia(int16 pin)
{
    
    while (!input(pin));//Esperar a que la se?al de Echo se active
    set_timer1(0);
    while (input(pin));
    return get_timer1()/58;
       
    
}


void emitoSonidoF_TDeAcuerdoALaDistancia(long distancia)
{
    if (distancia > 40 && distancia < 50){
        output_high(PIN_A2);
        delay_ms(600);
        output_low(PIN_A2);
        delay_ms(400);
    //Sound_Play(500, 90); //emite sonido de 500 HZ por 90 ms
    } else if (distancia > 20 && distancia <= 40){
        output_high(PIN_A2);
        delay_ms(400);
        output_low(PIN_A2);
        delay_ms(200);
        output_high(PIN_A2);
        delay_ms(400);
        output_low(PIN_A2);
        delay_ms(200);
    //Sound_Play(800, 110);  //emite sonido de 800 HZ por 90 ms
    } else if (distancia <= 20){
        output_high(PIN_A2);
        delay_ms(200);
        output_low(PIN_A2);
        delay_ms(100);
        output_high(PIN_A2);
        delay_ms(200);
        output_low(PIN_A2);
        delay_ms(100);
        output_high(PIN_A2);
        delay_ms(200);
        output_low(PIN_A2);
        delay_ms(100);
        //Sound_Play(1000, 220);  //emite sonido de 1000 HZ por 90 ms
    }else{
      output_low(PIN_A2);
      delay_ms(100);
    }
}


void emitoSonidoLateralDeAcuerdoALaDistancia(long distancia)
{
    if (distancia > 40 && distancia < 50){
        output_high(PIN_A4);
        delay_ms(600);
        output_low(PIN_A4);
        delay_ms(400);
    //Sound_Play(500, 90); //emite sonido de 500 HZ por 90 ms
    } else if (distancia > 20 && distancia <= 40){
        output_high(PIN_A4);
        delay_ms(400);
        output_low(PIN_A4);
        delay_ms(200);
        output_high(PIN_A4);
        delay_ms(400);
        output_low(PIN_A4);
        delay_ms(200);
    //Sound_Play(800, 110);  //emite sonido de 800 HZ por 90 ms
    } else if (distancia <= 20){
        output_high(PIN_A4);
        delay_ms(200);
        output_low(PIN_A4);
        delay_ms(100);
        output_high(PIN_A4);
        delay_ms(200);
        output_low(PIN_A4);
        delay_ms(100);
        output_high(PIN_A4);
        delay_ms(200);
        output_low(PIN_A4);
        delay_ms(100);
        //Sound_Play(1000, 220);  //emite sonido de 1000 HZ por 90 ms
    }else{
      output_low(PIN_A4);
      delay_ms(100);
    }
}

void main() 
{
    inicialisarPuertos();
    //Sound_Init(&GPIO, 2); //le digo que el pin por el que va a salir  el sonido es el 2 (contando desde 0)

    while(1){
       distanciaA=0;
       distanciaB=0;
       setup_timer_1(T1_INTERNAL|T1_DIV_BY_1);
       triggearSensor();
       distanciaA=siTengoEcoCalculoDistancia(PIN_A1);
       delay_ms(100);
       triggearSensor();
       distanciaB=siTengoEcoCalculoDistancia(PIN_A3);
       setup_timer_1(T1_DISABLED);
       emitoSonidoF_TDeAcuerdoALaDistancia(distanciaA);
       emitoSonidoLateralDeAcuerdoALaDistancia(distanciaB);
       
    }
}


